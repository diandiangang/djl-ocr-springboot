package com.example.djlocrspringboot.djlocr;

/**
 * Copyright © XXX. All rights reserved.
 *
 * @Title GapHammingDistance
 * @Prject: Hyperledger-Fabric-V1-Test
 * @Package: com.example.hyperledgerfabricv1test
 * @Description:
 * @Author: liyl
 * @Date: 2023/5/19 15:23
 * @version:
 */
public class GapHammingDistance {
    
    public static int getGapHammingDistance(String str1, String str2) {
        int distance = 0;
        int len1 = str1.length();
        int len2 = str2.length();
        int maxLength = Math.max(len1, len2);
        
        for (int i = 0; i < maxLength; i++) {
            char char1 = i < len1 ? str1.charAt(i) : ' ';
            char char2 = i < len2 ? str2.charAt(i) : ' ';
            
            if (char1 != char2) {
                distance++;
            }
        }
        
        return distance;
    }
    
    // public static void main(String[] args) {
    //     String str1 = "Hello World";
    //     String str2 = "Hello Java";
    //
    //     int gapHammingDistance = getGapHammingDistance(str1, str2);
    //     // System.out.println("Gap Hamming Distance: " + gapHammingDistance);
    // }
}
