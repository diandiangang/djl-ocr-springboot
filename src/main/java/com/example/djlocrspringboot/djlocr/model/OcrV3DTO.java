package com.example.djlocrspringboot.djlocr.model;

import ai.djl.repository.zoo.ZooModel;
import lombok.Data;

/**
 * Copyright © XXX. All rights reserved.
 *
 * @Title OcrV3DTO
 * @Prject: djl-ocr-springboot
 * @Package: com.example.djlocrspringboot.djlocr.model
 * @Description:
 * @Author: liyl
 * @Date: 2023/6/16 10:30
 * @version:
 */
@Data
public class OcrV3DTO {
    private ZooModel detectionModel;
    private ZooModel recognitionModel;
}
