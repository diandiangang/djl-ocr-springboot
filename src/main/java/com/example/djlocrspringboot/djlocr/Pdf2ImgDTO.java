package com.example.djlocrspringboot.djlocr;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Copyright © XXX. All rights reserved.
 *
 * @Title Pdf2ImgDTO
 * @Prject: oim-tools
 * @Package: com.guigetech.oim.tools.utils.ass.file
 * @Description: PDF 2 IMG 出参实体
 * @Author: liyl
 * @Date: 2022/12/21 16:13
 * @version:
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Pdf2ImgDTO implements Serializable {
    
    /**
     * 页码
     */
    private int pageNumber;
    /**
     * 图片二进制流
     */
    private byte[] pdfByte;
    /**
     * 图片类型
     */
    private String imageType;
}
