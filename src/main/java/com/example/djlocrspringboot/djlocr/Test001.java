package com.example.djlocrspringboot.djlocr;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Base64;

/**
 * Copyright © XXX. All rights reserved.
 *
 * @Title Test001
 * @Prject: Hyperledger-Fabric-V1-Test
 * @Package: com.example.hyperledgerfabricv1test
 * @Description:
 * @Author: liyl
 * @Date: 2023/5/19 10:21
 * @version:
 */
public class Test001 {
    // public static void main(String[] args) {
    //     String filePath = "D:\\Guige\\file\\测试估值表\\941133华泰杭瑞9号集合资产管理计划_估值表_20220104_840100.xls"; // 替换为实际文件的路径
    //
    //     try {
    //         String base64String = convertFileToBase64(filePath);
    //         // System.out.println("=======start=======");
    //         // System.out.println(base64String);
    //         // System.out.println("========end========");
    //     } catch (IOException e) {
    //         e.printStackTrace();
    //     }
    // }
    
    public static String convertFileToBase64(String filePath) throws IOException {
        File file = new File(filePath);
        FileInputStream fileInputStream = new FileInputStream(file);
        
        byte[] fileBytes = new byte[(int) file.length()];
        fileInputStream.read(fileBytes);
        fileInputStream.close();
        
        return Base64.getEncoder().encodeToString(fileBytes);
    }
}
