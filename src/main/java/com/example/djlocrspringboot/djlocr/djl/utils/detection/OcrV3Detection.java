package com.example.djlocrspringboot.djlocr.djl.utils.detection;

import java.util.concurrent.ConcurrentHashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import ai.djl.Device;
import ai.djl.modality.cv.Image;
import ai.djl.ndarray.NDList;
import ai.djl.repository.zoo.Criteria;
import ai.djl.training.util.ProgressBar;

@Component
public class OcrV3Detection {
    
    private static final Logger logger = LoggerFactory.getLogger(OcrV3Detection.class);
    
    public OcrV3Detection() {
    }
    
    public Criteria<Image, NDList> detectCriteria() {
        // ClassPathResource resource = new ClassPathResource("static/ch_PP-OCRv3_det_infer.zip");
        // System.out.println(resource.getAbsolutePath());
        Criteria<Image, NDList> criteria = Criteria.builder()
                .optEngine("PaddlePaddle")
                .optDevice(Device.gpu())
                .setTypes(Image.class, NDList.class)
                .optModelUrls("/guige/ch_PP-OCRv3_det_infer.zip")
                .optTranslator(new OCRDetectionTranslator(new ConcurrentHashMap<String, String>()))
                .optProgress(new ProgressBar())
                .build();
    
        return criteria;
    }
}
