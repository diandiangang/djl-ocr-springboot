package com.example.djlocrspringboot.djlocr;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import com.itextpdf.pdfocr.OcrPdfCreator;
import com.itextpdf.pdfocr.tesseract4.Tesseract4LibOcrEngine;
import com.itextpdf.pdfocr.tesseract4.Tesseract4OcrEngineProperties;
import com.itextpdf.kernel.pdf.PdfWriter;

/**
 * Copyright © XXX. All rights reserved.
 *
 * @Title ItextOCR
 * @Prject: Hyperledger-Fabric-V1-Test
 * @Package: com.example.hyperledgerfabricv1test
 * @Description:
 * @Author: liyl
 * @Date: 2023/5/22 9:11
 * @version:
 */
public class ItextOCR {
    static final Tesseract4OcrEngineProperties tesseract4OcrEngineProperties = new Tesseract4OcrEngineProperties();
    private static List LIST_IMAGES_OCR = Arrays.asList(new File("C:\\Users\\liyl\\Desktop\\img-516163551.tif")); //replace with the image file name you have uploaded
    private static String OUTPUT_PDF = "/myfiles/hello.pdf";

    // public static void main(String[] args) throws IOException {
    //     final Tesseract4LibOcrEngine tesseractReader = new Tesseract4LibOcrEngine(tesseract4OcrEngineProperties);
    //     tesseract4OcrEngineProperties.setPathToTessData(new File("/data-files/"));
    //
    //     OcrPdfCreator ocrPdfCreator = new OcrPdfCreator(tesseractReader);
    //     try (PdfWriter writer = new PdfWriter(OUTPUT_PDF)) {
    //         ocrPdfCreator.createPdf(LIST_IMAGES_OCR, writer).close();
    //     }
    // }
}
