package com.example.djlocrspringboot.controller;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.example.djlocrspringboot.djlocr.PDFSimilarityComparator;
import com.example.djlocrspringboot.djlocr.djl.OcrV3RecognitionExample;

/**
 * Copyright © XXX. All rights reserved.
 *
 * @Title TestOcrController
 * @Prject: djl-ocr-springboot
 * @Package: com.example.djlocrspringboot.controller
 * @Description:
 * @Author: liyl
 * @Date: 2023/5/23 10:10
 * @version:
 */
@RestController
@RequestMapping("djl")
public class TestOcrController {
    
    @Autowired
    private OcrV3RecognitionExample ocrV3RecognitionExample;
    @Autowired
    private PDFSimilarityComparator pdfSimilarityComparator;
    
    @PostMapping("/ocr")
    public String changeBaseHere(MultipartFile file, Integer dpi, Integer threadNumber) {
        try {
            String s = pdfSimilarityComparator.testSpringBootDJLOcr(file.getBytes(), dpi, threadNumber);
            return s;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
