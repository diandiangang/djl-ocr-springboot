package com.example.djlocrspringboot;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.example.djlocrspringboot.djlocr.djl.OcrV3RecognitionExample;


@SpringBootApplication
public class DjlOcrSpringbootApplication {
    
    @Autowired
    private OcrV3RecognitionExample ocrV3RecognitionExample;
    
    private static OcrV3RecognitionExample theOcrV3RecognitionExample;
    @PostConstruct
    private void init() {
        theOcrV3RecognitionExample = ocrV3RecognitionExample;
    }
    public static void main(String[] args) {
        SpringApplication.run(DjlOcrSpringbootApplication.class, args);
        // System.setProperty("DJL_ENGINE_TYPE", "PaddlePaddle");
        // System.setProperty("DJL_PADDLE_MKLDNN", "true");
        // System.setProperty("DJL_PADDLE_NUM_THREADS", "5"); // 设置所需的线程数
        // theOcrV3RecognitionExample.init();
        System.out.println("DjlOcrSpringbootApplication is start...");
    }
    
}
